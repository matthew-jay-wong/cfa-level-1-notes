---
title: "1.3 Probability Concepts"
author: "Matthew Jay Wong"
date: "2022-11-11"
---

## Probability Concepts & Odds Ratio

- Random Variable
  - Quantity whose future outcome is unknown
- Outcome
  - Possible value of a random variable
- Event
  - Specified set of outcomes
- Probability
  - Number between [0, 1] that measures the chance a stated event will occur
- Mutually Exclusive
  - Only one event from set of events can happen at one time.
- Exhaustive
  - Set of possible events covers all possible events
  
### Probability Types

- Objective Probabilities
  - Empirical
    - Estimated as a relative frequency of occurrence based on historical data
  - A Priori
    - Estimations are deduced by reasoning based on logical analysis rather then on observations or personal judgement
- Subjective
  - Estimations are drawn from personal or subjective judgement.

### Probability Stated as Odds

- $\text{Odds for E} = \frac{P(E)}{1 - P(E)}$
- $\text{Odds against E} = \frac{1 - P(E)}{P(E)}$

## Conditional & Joint Probability

- Multiplication rule 
  - Dependent Events
    - $P(A \wedge B) = P(A|B)P(B)$
  - Independent Events
    - $P(A|B) = P(A)$
    - $P(B|A) = P(B)$
    - $P(A \wedge B) = P(A)P(B)$
- Addition rule
  - $P(A \vee B) = P(A) + P(B) - P(A \wedge B)$

### Total Probability

$$
P(A) = P(A|S)P(S) + P(A|\neg S)P(\neg S) \\
P(A) = \sum{P(A|S_i)P(S_i)}_{i = 1}^{n}
$$
## Expected Value & Variance

- Expected Value
  - $E(X) = \sum{P(X_I)X_i}_{i = 1}^{n}$
- Conditional Expected Value
  - $E(X|S) = \sum{P(X_i|S)X_i}_{i = 1}^{n}$
- Total Probability for Expected Value
  - $E(X) = E(X|S)P(S) + E(X|\neg S)P(\neg S)$
  - $E(X) = \sum{E(X|S_i)P(S_i)}_{i = 1}^{n}$
- Variance
  - $\sigma^2 = \sum{P(X_i)(X_i - E(X))^2}_{i = 1}^{n}$

## Portfolio Expected Return & Variance of Return

- Expected Return on Portfolio
  - Weighted average pf tje expected returns of each portfolio component
- Covariance of two random variables
  - $Cov(X,Y) = \frac{\sum{(X_i - \overline{X})(Y_i - \overline{Y})}_{i = 1}^{n}}{n - 1}$
- Variance of Portfolio Returns
  - $\sigma^2(R_p) = \sum{\sum{w_i w_j Cov(R_i, R_j)}_{j=1}^{n}}_{i=1}^{n}$
  
## Bayes' Formula

$$
P(Event | Info) = \frac{P(Info|Event)}{P(Info)}P(Event)
$$

## Principals of Counting

- Multinomial Formula (General Formula for Labeling Problems)
  - The number of ways that $n$ objects can be labeled with $k$ different labels, with $n_1$ as the first type, $n_2$ of the second type, and so on, with $\sum{n_i}_{i = 1}^{k} = n$
  - $\frac{n!}{\prod{n_i!}_{i = 1}^{k}}$
- Combination Formula (Binomial Formula)
  - The multinomial formula but with only two labels.
  - The number of ways to choose $r$ objects from a total of $n$ objects, when the order in which the $r$ objects chosen do not matter.
  - $_{n}C_{r} = \binom{n}{r} = \frac{n!}{(n - r)!r!}$
- Permutation Formula
  - The number of ways to choose $r$ objects from a total of $n$ objects, when the order in which the $r$ objects chosen do matter.
  - $_{n}P_{r} = \frac{n!}{(n - r)!}$
